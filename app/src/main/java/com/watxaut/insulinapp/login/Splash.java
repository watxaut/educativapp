package com.watxaut.insulinapp.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;

import com.watxaut.insulinapp.R;
import com.watxaut.insulinapp.helper.GlobalFunctions;
import com.watxaut.insulinapp.helper.Singleton;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class Splash extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        GlobalFunctions globalFunctions = new GlobalFunctions(this);
        /*
        switch (globalFunctions.checkAppStart()) {

            case NORMAL:
                //Usuari no ha ni fet upgrade ni instalat per primer cop
                break;
            case FIRST_TIME_VERSION:
                //Usuari ha fet upgrade
                break;
            case FIRST_TIME:
                //primer cop de lusuari

                break;
            default:
                break;
        }*/

        //instancia les paletes totes negre per evitar null pointer si no te temps
        globalFunctions.setPalettesNegres();

        //asincronament assigna les paletes cada una al seu color
        globalFunctions.fesPalettes();

        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {

                if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.LOLLIPOP) {
                    Intent intent = new Intent(Splash.this, StartupActivity.class);
                    final View frameLayout = findViewById(R.id.framelayout);

                    ActivityOptionsCompat options = ActivityOptionsCompat.
                            makeSceneTransitionAnimation(Splash.this, frameLayout, "sharedElement");
                    startActivity(intent, options.toBundle());
                    Splash.this.finishAfterTransition();
                }else {
                    startActivity(new Intent(Splash.this, StartupActivity.class));

                    Splash.this.finish();

                }
            }
        }, secondsDelayed * 1500);
    }
}
