package com.watxaut.insulinapp.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;

import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;

import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;

import android.widget.RelativeLayout;


import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.rey.material.app.ThemeManager;
import com.watxaut.insulinapp.helper.ParseErrorHandler;
import com.watxaut.insulinapp.mainScreen.MainActivity;
import com.watxaut.insulinapp.R;
import com.watxaut.insulinapp.helper.GlobalFunctions;
import com.watxaut.insulinapp.helper.Singleton;

import java.util.List;


public class LogInScreen extends AppCompatActivity {

    //Variables Globals
    GlobalFunctions globalFunctions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeManager.init(this, 1, 0, null);
        setContentView(R.layout.login_v2);


        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.LOLLIPOP) {
            final View explode = findViewById(R.id.explode_layout);

            explode.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    explode.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                    explode.setVisibility(View.VISIBLE);

                    circularReveal(explode);
                }
            });



        }

        globalFunctions = new GlobalFunctions(this);

        final com.rey.material.widget.EditText contrassenyaUsuari = (com.rey.material.widget.EditText) findViewById((R.id.contrassenyaLogIn));
        contrassenyaUsuari.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    contrassenyaUsuari.setError(null);
            }
        });
        final com.rey.material.widget.EditText nomUsuari = (com.rey.material.widget.EditText) findViewById((R.id.nomUsuariLogIn));
        nomUsuari.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    nomUsuari.setError(null);
            }
        });

    }

    public void circularReveal(final View myView){

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;

        // get the center for the clipping circle
        int cx = width/2;
        int cy = height;

        // get the initial radius for the clipping circle
        int initialRadius = myView.getWidth();

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                myView.setVisibility(View.INVISIBLE);
            }
        });

        // start the animation
        anim.start();

    }


    //-------------------------------------------------------------Usuari loggejat------------------------------------------------------------------//

    public void logIn(View view){
        final com.rey.material.widget.EditText nomUsuari = (com.rey.material.widget.EditText) findViewById(R.id.nomUsuariLogIn);
        final com.rey.material.widget.EditText contrassenyaUsuari = (com.rey.material.widget.EditText) findViewById((R.id.contrassenyaLogIn));

        final RelativeLayout progressScreen = (RelativeLayout) findViewById(R.id.progressLayout);
        //final TextView errorLogIn = (TextView) findViewById(R.id.errorLogIn);

        //Si cap de les dos es ""
        if (!nomUsuari.getText().toString().equals("") || !contrassenyaUsuari.getText().toString().equals("")) {
            hide_keyboard(this);
            nomUsuari.clearFocus();
            contrassenyaUsuari.clearFocus();

            //Check si hi ha connexio (pero aixo no vol dir que es pugui connectar al servidor)
            if (globalFunctions.isOnline()) {

                progressScreen.setVisibility(View.VISIBLE);
                ParseUser.logInInBackground(nomUsuari.getText().toString(), contrassenyaUsuari.getText().toString(), new LogInCallback() {
                    public void done(ParseUser user, ParseException e) {
                        if (user != null) {
                            // Hooray! The user is logged in.
                            final Singleton singleton = Singleton.getInstance();
                            singleton.setNom(user.getString("Nom"));
                            singleton.setCognoms(user.getString("Cognoms"));
                            singleton.setNickname(user.getUsername());
                            singleton.setNivellActual(user.getInt("NivellActual"));
                            singleton.setAvatar(user.getString("Avatar"));
                            singleton.setExperiencia(user.getInt("Experiencia"));
                            singleton.setNivellUsuari(user.getInt("NivellUsuari"));
                            singleton.setNivellsPassats((List<Integer>) (Object) user.getList("NivellsPassats"));

                            ParseQuery<ParseObject> query = ParseQuery.getQuery("Nivells");
                            query.orderByAscending("Nivell");
                            query.findInBackground(new FindCallback<ParseObject>() {
                                public void done(List<ParseObject> llistaParse, ParseException e) {
                                    if (e == null) {
                                        singleton.setLlistaNivells(llistaParse);
                                    } else {
                                        ParseErrorHandler.handleParseError(e, LogInScreen.this);
                                    }
                                }
                            });

                            ParseQuery<ParseObject> query2 = ParseQuery.getQuery("Constants");
                            query2.findInBackground(new FindCallback<ParseObject>() {
                                public void done(List<ParseObject> listConstants, ParseException e) {
                                    if (e == null) {
                                        for (ParseObject constant: listConstants){
                                            String nom = constant.getString("Constant");
                                            if (nom.equals("RangPunts")){
                                                singleton.setLlistaRangPunts((List<Integer>) (Object) constant.getList("ArrayValue"));
                                            }else if (nom.equals("DescendenciaNivells")){
                                                singleton.setLlistaWorlds((List<List<Integer>>) (Object) constant.getList("ArrayValue"));
                                            } else if (nom.equals("RangNoms")){
                                                singleton.setLlistaRangNoms((List<String>) (Object) constant.getList("ArrayValue"));
                                            }else if(nom.equals("Ascendencia")){
                                                globalFunctions.fesDescendenciaiAscendencia((List<List<Integer>>) (Object) constant.getList("ArrayValue"));
                                            }

                                        }


                                        startActivity(new Intent(LogInScreen.this, MainActivity.class));
                                        progressScreen.setVisibility(View.GONE);
                                        overridePendingTransition(R.anim.activity_slide_start_enter, R.anim.activity_scale_start_exit);
                                        finish();
                                    } else {
                                        ParseErrorHandler.handleParseError(e, LogInScreen.this);
                                    }
                                }
                            });


                        } else {
                            contrassenyaUsuari.setError(getResources().getString(R.string.parametresIncorrectes));
                            progressScreen.setVisibility(View.GONE);
                            Log.i("InsulinApp", e.getMessage());
                        }
                    }
                });
            }else{
                //Si no hi ha connexio a internet
                globalFunctions.fesConnexioBuilder().show();
            }
        }else{
            //Si estan els dos camps buits
            nomUsuari.setError(getResources().getString(R.string.campBuit));
            contrassenyaUsuari.setError(getResources().getString(R.string.campBuit));
        }
    }


    //-------------------------------------------------------- Altres ----------------------------------------------------------------//

    public static void hide_keyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if(view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(LogInScreen.this, StartupActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
