package com.watxaut.insulinapp.login;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.RelativeLayout;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import com.rey.material.app.ThemeManager;
import com.watxaut.insulinapp.helper.ParseErrorHandler;
import com.watxaut.insulinapp.mainScreen.MainActivity;
import com.watxaut.insulinapp.R;
import com.watxaut.insulinapp.helper.GlobalFunctions;
import com.watxaut.insulinapp.helper.Singleton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.GregorianCalendar;
import java.util.List;

public class NewUserScreen extends AppCompatActivity {

    NumberPicker dies;
    NumberPicker mesos;
    NumberPicker anys;
    com.rey.material.widget.EditText  nom;
    com.rey.material.widget.EditText  cognoms;
    com.rey.material.widget.EditText nickname;
    com.rey.material.widget.EditText email;
    com.rey.material.widget.EditText  contrassenya;
    com.rey.material.widget.EditText  repetirContrassenya;
    GlobalFunctions globalFunctions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeManager.init(this, 1, 0, null);
        setContentView(R.layout.new_user_screen);

        globalFunctions = new GlobalFunctions(this);

        nom = (com.rey.material.widget.EditText ) findViewById(R.id.nom);
        cognoms = (com.rey.material.widget.EditText ) findViewById(R.id.cognoms);
        nickname = (com.rey.material.widget.EditText) findViewById(R.id.nickname);
        email = (com.rey.material.widget.EditText) findViewById(R.id.email);
        contrassenya = (com.rey.material.widget.EditText ) findViewById(R.id.contrassenya);
        repetirContrassenya = (com.rey.material.widget.EditText ) findViewById(R.id.repeteixContrassenya);

        dies = (NumberPicker) findViewById(R.id.dies);
        mesos = (NumberPicker) findViewById(R.id.mesos);
        anys = (NumberPicker) findViewById(R.id.anys);

        dies.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        mesos.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        anys.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        dies.setMaxValue(31);
        dies.setMinValue(1);
        mesos.setMaxValue(12);
        mesos.setMinValue(1);
        anys.setMaxValue(2015);
        anys.setMinValue(1970);

        anys.setValue(2005);

        anys.setWrapSelectorWheel(false);

        anys.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i2) {
                if (isLeapYear(i2)) {
                    //L'any te 366 dies
                    if (mesos.getValue() == 2) {
                        dies.setMaxValue(29);
                    }
                } else {
                    //Es un any comu
                    if (mesos.getValue() == 2) {
                        dies.setMaxValue(28);
                    }
                }
            }
        });

        mesos.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i2) {
                if (i2 == 1 || i2 == 3 || i2 == 5 || i2 == 7 || i2 == 8 || i2 == 10 || i2 == 12) {
                    //Tenen 31 dies
                    dies.setMaxValue(31);

                } else if (i2 == 2) {
                    //Si es Febrer
                    if (isLeapYear(anys.getValue())) {
                        //Si es leap Year
                        dies.setMaxValue(29);

                    } else {
                        dies.setMaxValue(28);
                    }

                } else {
                    //Tots els altres tenen 30
                    dies.setMaxValue(30);
                }
            }
        });

        nom.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    nom.setError(null);
            }
        });
        cognoms.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    cognoms.setError(null);
            }
        });
        nickname.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    nickname.setError(null);
            }
        });
        contrassenya.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    contrassenya.setError(null);
            }
        });
        repetirContrassenya.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    repetirContrassenya.setError(null);
            }
        });
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus)
                    email.setError(null);
            }
        });

    }

    public static boolean isLeapYear(int year) {
        return new GregorianCalendar(year, 1, 1).isLeapYear(year);
    }

    public void registrarNouUsuari(View view){

        nom.clearFocus();
        cognoms.clearFocus();
        nickname.clearFocus();
        contrassenya.clearFocus();
        repetirContrassenya.clearFocus();
        email.clearFocus();

        //Mira si algun dels camps falta
        if (nom.getText().toString().equals("") || cognoms.getText().toString().equals("") || nickname.getText().toString().equals("") || contrassenya.getText().toString().equals("") || repetirContrassenya.getText().toString().equals("")){

            if (nom.getText().toString().equals("")){
                nom.requestFocus();
                nom.setError(getResources().getString(R.string.campBuit));
            }
            if (cognoms.getText().toString().equals("")){
                cognoms.requestFocus();
                cognoms.setError(getResources().getString(R.string.campBuit));
            }
            if (nickname.getText().toString().equals("")){
                nickname.requestFocus();
                nickname.setError(getResources().getString(R.string.campBuit));
            }
            if (contrassenya.getText().toString().equals("")){
                contrassenya.requestFocus();
                contrassenya.setError(getResources().getString(R.string.campBuit));
            }
            if (repetirContrassenya.getText().toString().equals("")){
                repetirContrassenya.requestFocus();
                repetirContrassenya.setError(getResources().getString(R.string.campBuit));
            }
        }else{

            //Mira si les contrassenyes son les mateixes
            if (contrassenya.getText().toString().equals(repetirContrassenya.getText().toString())){


                globalFunctions.fesSimpleBuilder(getString(R.string.privacitat), getString(R.string.descripcioPrivacitat))
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {


                final RelativeLayout progressScreen = (RelativeLayout) findViewById(R.id.progressLayoutNewUser);
                progressScreen.setVisibility(View.VISIBLE);

                //Comprovem si te connexio a internet
                if (globalFunctions.isOnline()) {

                    final String avatar = globalFunctions.retornaRandomString();

                    //Guarda el nickname per despres
                    final Singleton singleton = Singleton.getInstance();
                    singleton.setNom(nom.getText().toString());
                    singleton.setCognoms(cognoms.getText().toString());
                    singleton.setNickname(nickname.getText().toString());
                    singleton.setAvatar(avatar);
                    singleton.setNivellActual(1);
                    singleton.setExperiencia(0);
                    singleton.setNivellUsuari(1);
                    singleton.setNivellsPassats(new ArrayList<Integer>());

                    //Crea nou usuari
                    ParseUser usuari = new ParseUser();
                    usuari.put("Nom", nom.getText().toString());
                    usuari.put("Cognoms", cognoms.getText().toString());
                    usuari.setUsername(nickname.getText().toString());
                    if (!email.getText().toString().equals("")) {
                        usuari.setEmail(email.getText().toString());
                    }
                    usuari.setPassword(contrassenya.getText().toString());
                    usuari.put("DataNaixement", dies.getValue() + "/" + mesos.getValue() + "/" + anys.getValue());
                    usuari.put("Categoria", "A determinar");
                    usuari.put("Diagnosi", "A determinar");
                    usuari.put("Puntuacio", 0);
                    usuari.put("NivellActual", 1);

                    usuari.put("Avatar", avatar);
                    usuari.put("Experiencia", 0);
                    usuari.put("NivellUsuari", 1);
                    usuari.put("NivellsPassats", new ArrayList<Integer>());
                    usuari.signUpInBackground(new SignUpCallback() {
                        public void done(ParseException e) {
                            if (e == null) {

                                ParseQuery<ParseObject> query = ParseQuery.getQuery("Nivells");
                                query.orderByAscending("Nivell");
                                query.findInBackground(new FindCallback<ParseObject>() {
                                    public void done(List<ParseObject> llistaParse, ParseException e) {
                                        if (e == null) {
                                            singleton.setLlistaNivells(llistaParse);
                                        } else {
                                            ParseErrorHandler.handleParseError(e, NewUserScreen.this);
                                        }
                                    }
                                });

                                ParseQuery<ParseObject> query2 = ParseQuery.getQuery("Constants");
                                query2.findInBackground(new FindCallback<ParseObject>() {
                                    public void done(List<ParseObject> listConstants, ParseException e) {
                                        if (e == null) {
                                            for (ParseObject constant: listConstants){
                                                String nom = constant.getString("Constant");
                                                if (nom.equals("RangPunts")){
                                                    singleton.setLlistaRangPunts((List<Integer>) (Object) constant.getList("ArrayValue"));

                                                }else if (nom.equals("DescendenciaNivells")){
                                                    singleton.setLlistaWorlds((List<List<Integer>>) (Object) constant.getList("ArrayValue"));
                                                } else if (nom.equals("RangNoms")){
                                                    singleton.setLlistaRangNoms((List<String>) (Object) constant.getList("ArrayValue"));
                                                } else if(nom.equals("Ascendencia")){
                                                    globalFunctions.fesDescendenciaiAscendencia((List<List<Integer>>) (Object) constant.getList("ArrayValue"));

                                                }
                                            }
                                            progressScreen.setVisibility(View.GONE);
                                            overridePendingTransition(R.anim.activity_slide_start_enter, R.anim.activity_scale_start_exit);
                                            startActivity(new Intent(NewUserScreen.this, MainActivity.class));
                                            finish();
                                        } else {
                                            ParseErrorHandler.handleParseError(e, NewUserScreen.this);
                                        }
                                    }
                                });


                            } else {
                                progressScreen.setVisibility(View.GONE);
                                // Sign up didn't succeed. Look at the ParseException
                                // to figure out what went wrong
                                if (e.toString().contains("username")) {
                                    //Usuari agafat
                                    nickname.setError(getResources().getString(R.string.usuariExisteix));
                                    nickname.requestFocus();
                                }
                                if (e.toString().contains("email")) {
                                    //Mail agafat LEL
                                    email.setError(getResources().getString(R.string.mailExisteix));
                                    email.requestFocus();
                                }
                                Log.i("SignUpParseError", "error: " + e);
                            }
                        }
                    });
                }else{
                    //No hi ha connexio a internet, fes builder
                    progressScreen.setVisibility(View.GONE);
                    globalFunctions.fesConnexioBuilder().show();
                }

                dialog.dismiss();}})

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();

            }else{
                //Les contrassenyes no coincideixen
                contrassenya.requestFocus();
                contrassenya.setError(getResources().getString(R.string.errorContrassenyaRepetida));
                repetirContrassenya.setError(getResources().getString(R.string.errorContrassenyaRepetida));


            }
        }
    }


    @Override
    public void onBackPressed() {

        //pregunta si vol sortir sense guardar
        globalFunctions.fesSimpleBuilder(getString(R.string.titolSortirNew), getString(R.string.cosSortirNew))
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(NewUserScreen.this, StartupActivity.class));
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        finish();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
    }

}


