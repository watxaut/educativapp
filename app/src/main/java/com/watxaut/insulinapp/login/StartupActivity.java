package com.watxaut.insulinapp.login;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseAnonymousUtils;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.rey.material.app.ThemeManager;
import com.watxaut.insulinapp.mainScreen.MainActivity;
import com.watxaut.insulinapp.R;
import com.watxaut.insulinapp.helper.GlobalFunctions;
import com.watxaut.insulinapp.helper.ParseErrorHandler;
import com.watxaut.insulinapp.helper.Singleton;

import java.util.ArrayList;
import java.util.List;


public class StartupActivity extends AppCompatActivity {


    GlobalFunctions globalFunctions;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.LOLLIPOP) {
            getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        }
        super.onCreate(savedInstanceState);
        ThemeManager.init(this, 1, 0, null);
        setContentView(R.layout.startup_v2);

        globalFunctions = new GlobalFunctions(this);
        
    }

    @Override
    protected void onResume() {
        super.onResume();
        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null) {
            ParseUser.logOutInBackground();
        }
    }

    //-----------------------------------------------------------------Usuari anònim---------------------------------------------------------------------//

    public void guestUser(View view){
        final RelativeLayout progressScreen = (RelativeLayout) findViewById(R.id.progressLayoutStartUp);
        progressScreen.setVisibility(View.VISIBLE);
        if (globalFunctions.isOnline()) {
            final String avatar = globalFunctions.retornaRandomString();
            ParseAnonymousUtils.logIn(new LogInCallback() {
                @Override
                public void done(ParseUser user, ParseException e) {
                    if (e != null) {
                        Toast.makeText(StartupActivity.this, getResources().getString(R.string.whoops), Toast.LENGTH_SHORT).show();
                        progressScreen.setVisibility(View.GONE);
                        ParseErrorHandler.handleParseError(e, StartupActivity.this);
                    } else {
                        user.put("Nom", "Anònim");
                        user.put("Avatar", avatar);
                        user.put("NivellActual", 1);
                        user.put("Puntuacio", 0);
                        user.put("Experiencia", 0);
                        user.put("NivellUsuari", 1);
                        user.put("NivellsPassats", new ArrayList<Integer>());
                        user.saveEventually();

                        final Singleton singleton = Singleton.getInstance();
                        singleton.setNom("Anònim");
                        singleton.setCognoms("");
                        singleton.setNickname("usuari convidat");
                        singleton.setNivellActual(1);
                        singleton.setAvatar(avatar);
                        singleton.setExperiencia(0);
                        singleton.setNivellUsuari(1);
                        singleton.setNivellsPassats(new ArrayList<Integer>());
                        Log.d("MyApp", "Anonymous user logged in.");

                        ParseQuery<ParseObject> query = ParseQuery.getQuery("Nivells");
                        query.orderByAscending("Nivell");
                        query.findInBackground(new FindCallback<ParseObject>() {
                            public void done(List<ParseObject> llistaParse, ParseException e) {
                                if (e == null) {
                                    singleton.setLlistaNivells(llistaParse);
                                } else {
                                    ParseErrorHandler.handleParseError(e, StartupActivity.this);
                                }
                            }
                        });

                        ParseQuery<ParseObject> query2 = ParseQuery.getQuery("Constants");
                        query2.findInBackground(new FindCallback<ParseObject>() {
                            public void done(List<ParseObject> listConstants, ParseException e) {
                                if (e == null) {
                                    for (ParseObject constant: listConstants){
                                        String nom = constant.getString("Constant");
                                        if (nom.equals("RangPunts")){
                                            singleton.setLlistaRangPunts((List<Integer>) (Object) constant.getList("ArrayValue"));
                                        }else if (nom.equals("DescendenciaNivells")){
                                            singleton.setLlistaWorlds((List<List<Integer>>) (Object) constant.getList("ArrayValue"));
                                        } else if (nom.equals("RangNoms")){
                                            singleton.setLlistaRangNoms((List<String>) (Object) constant.getList("ArrayValue"));
                                        } else if(nom.equals("Ascendencia")){
                                            globalFunctions.fesDescendenciaiAscendencia((List<List<Integer>>) (Object) constant.getList("ArrayValue"));
                                        }
                                    }
                                    progressScreen.setVisibility(View.GONE);
                                    startActivity(new Intent(StartupActivity.this, MainActivity.class));
                                    overridePendingTransition(R.anim.activity_slide_start_enter, R.anim.activity_scale_start_exit);
                                    finish();
                                } else {
                                    ParseErrorHandler.handleParseError(e, StartupActivity.this);
                                }
                            }
                        });
                    }
                }
            });
        }else{
            //No hi ha connexio a internet
            progressScreen.setVisibility(View.GONE);
            globalFunctions.fesConnexioBuilder().show();
        }
    }

    public void vesLogIn(View view){
        if (Build.VERSION.SDK_INT >=Build.VERSION_CODES.LOLLIPOP) {
            circularReveal();
        }else{
            startActivity(new Intent(StartupActivity.this, LogInScreen.class));
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            finish();
        }
    }


    //--------------------------------------------------------------Nou Usuari------------------------------------------------------------//

    public void newUser(View view){
        //startActivity(new Intent(StartupActivity.this, NewUserScreen.class));
        startActivity(new Intent(StartupActivity.this, NewUserScreen.class));

        overridePendingTransition(R.anim.activity_slide_start_enter, R.anim.activity_scale_start_exit);
        finish();
        //startActivity(new Intent(LogInScreen.this, AppTourNewUser.class));
    }

    @Override
    public void onBackPressed() {
        finish();

    }

    public void circularReveal(){

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        // previously invisible view
        final View myView = findViewById(R.id.explode_layout);

        // get the center for the clipping circle
        int cx = width/2;
        int cy = height;

        // get the final radius for the clipping circle
        int finalRadius = myView.getHeight();

        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);

        // make the view visible and start the animation
        myView.setVisibility(View.VISIBLE);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);

                startActivity(new Intent(StartupActivity.this, LogInScreen.class));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();

            }

        });
        anim.start();

    }
}
