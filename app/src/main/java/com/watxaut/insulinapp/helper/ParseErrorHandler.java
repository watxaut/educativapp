package com.watxaut.insulinapp.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.watxaut.insulinapp.login.LogInScreen;
import com.watxaut.insulinapp.R;


public class ParseErrorHandler {
    public static void handleParseError(ParseException e, Context context) {
        switch (e.getCode()) {
            case ParseException.INVALID_SESSION_TOKEN: handleInvalidSessionToken(context);
                break;
            case ParseException.TIMEOUT: handleTimeOut(context);
                break;
            default: handleAnyOtherException(context);
                break;

                // Other Parse API errors that you want to explicitly handle
        }
    }

    private static void handleInvalidSessionToken(final Context context) {
        //--------------------------------------
        // Option 1: Show a message asking the user to log out and log back in.
        //--------------------------------------
        // If the user needs to finish what they were doing, they have the opportunity to do so.
        //
        // new AlertDialog.Builder(getActivity())
        //   .setMessage("Session is no longer valid, please log out and log in again.")
        //   .setCancelable(false).setPositiveButton("OK", ...).create().show();

        //--------------------------------------
        // Option #2: Show login screen so user can re-authenticate.
        //--------------------------------------
        // You may want this if the logout button could be inaccessible in the UI.
        //
        new android.support.v7.app.AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.titolSessioExpirat))
                .setMessage(context.getString(R.string.cosSessioExpirat))
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        context.startActivity(new Intent(context, LogInScreen.class));
                        //overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }
                })
                .show();
    }
    private static void handleTimeOut(final Context context) {
        //--------------------------------------
        // Option 1: Show a message asking the user to log out and log back in.
        //--------------------------------------
        // If the user needs to finish what they were doing, they have the opportunity to do so.
        //
        // new AlertDialog.Builder(getActivity())
        //   .setMessage("Session is no longer valid, please log out and log in again.")
        //   .setCancelable(false).setPositiveButton("OK", ...).create().show();

        //--------------------------------------
        // Option #2: Show login screen so user can re-authenticate.
        //--------------------------------------
        // You may want this if the logout button could be inaccessible in the UI.
        //
        new android.support.v7.app.AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.titolTimeout))
                .setMessage(context.getString(R.string.cosTimeout))
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        context.startActivity(new Intent(context, LogInScreen.class));
                        //overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }
                })
                .show();
    }    private static void handleAnyOtherException(final Context context) {
        //--------------------------------------
        // Option 1: Show a message asking the user to log out and log back in.
        //--------------------------------------
        // If the user needs to finish what they were doing, they have the opportunity to do so.
        //
        // new AlertDialog.Builder(getActivity())
        //   .setMessage("Session is no longer valid, please log out and log in again.")
        //   .setCancelable(false).setPositiveButton("OK", ...).create().show();

        //--------------------------------------
        // Option #2: Show login screen so user can re-authenticate.
        //--------------------------------------
        // You may want this if the logout button could be inaccessible in the UI.
        //
        ParseUser.logOut();
        new android.support.v7.app.AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.whoops))
                .setMessage(context.getString(R.string.cosWhoops))
                .setCancelable(false)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        context.startActivity(new Intent(context, LogInScreen.class));
                        //overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    }
                })
                .show();
    }


}
