package com.watxaut.insulinapp.helper;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import com.parse.Parse;

public class App extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
                Parse.enableLocalDatastore(App.this);
                Parse.initialize(App.this, "mSl6pmfvo9XqZzsdW3RzGKbKMclsR5X76K67oAkI", "vCyVlkNmymmzCBUa80lLjl3kbxQUXsh1NjtjU34k");
                Log.i("AppInsulinUP", "Done initializing Parse");
            }
        });
    }
}

