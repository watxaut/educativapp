package com.watxaut.insulinapp.helper;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.parse.ParseUser;
import com.watxaut.insulinapp.R;

import java.util.List;
import java.util.Map;


public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder> {

    Context mContext;
    List<ParseUser> mList;
    Map<String,Integer> mMapColors;
    GlobalFunctions globalFunctions;
    OnItemClickListener mItemClickListener;
    public static int posicioLow = 15;

    // 2

    public ListAdapter(Context context, List<ParseUser> list, Map<String,Integer> listColors) {
        this.mContext = context;
        this.mList = list;
        this.mMapColors = listColors;
        globalFunctions = new GlobalFunctions(context);
    }

    // 3
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout placeHolder;
        public LinearLayout placeNameHolder;
        public TextView placeName;
        public ImageView placeImage;
        public TextView posicio;
        public TextView puntuacio;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);
            switch(viewType) {
                case 0:
                    placeHolder = (LinearLayout) itemView.findViewById(R.id.mainHolder);
                    placeName = (TextView) itemView.findViewById(R.id.placeName);
                    placeNameHolder = (LinearLayout) itemView.findViewById(R.id.placeNameHolder);
                    placeImage = (ImageView) itemView.findViewById(R.id.placeImage);
                    posicio = (TextView) itemView.findViewById(R.id.posicio);
                    puntuacio = (TextView) itemView.findViewById(R.id.puntuacio);
                    placeHolder.setOnClickListener(this);
                    break;
                case 1:
                    placeName = (TextView) itemView.findViewById(R.id.placeName);
                    placeNameHolder = (LinearLayout) itemView.findViewById(R.id.placeNameHolder);
                    posicio = (TextView) itemView.findViewById(R.id.posicio);
                    puntuacio = (TextView) itemView.findViewById(R.id.puntuacio);
                    placeNameHolder.setOnClickListener(this);
                    break;
            }

        }

        @Override
        public void onClick(View v) {
            if (mItemClickListener != null) {
                mItemClickListener.onItemClick(itemView, getPosition());
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        int type = 0;

        if (position>=posicioLow){
            //return type 1, els loosers
            type = 1;
        }
        return type;
    }


    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    // 2
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case 0:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_highscores, parent, false), viewType);
            case 1:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_highscores_low, parent, false), viewType);
            default:
                return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.card_highscores_low, parent, false), viewType);
        }
    }

    // 3
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final ParseUser user = mList.get(position);
        final String name;
        if(user.get("Nom").equals("Anònim")){
            name = "Anònim";
        }else{
            name = user.getUsername();
        }
        holder.puntuacio.setText(String.format("%1$d", user.getInt("Puntuacio")));
        holder.posicio.setText(String.format("%1$d" + getOrdinal(position + 1), position + 1));
        holder.placeName.setText(name);
        int d = globalFunctions.passaDrawable(user.getString("Avatar"));

        if (!(position>=posicioLow)) {
            holder.placeImage.setImageDrawable(ContextCompat.getDrawable(mContext, d));
        }
            holder.placeNameHolder.setBackgroundColor(mMapColors.get(user.getString("Avatar")));
    }
    public String getOrdinal(int posicio){
        String ordinal;
        switch (posicio){
            case 1: ordinal = "st";
                break;
            case 2: ordinal = "nd";
                break;
            case 3: ordinal = "rd";
                break;
            default: ordinal = "th";
                break;
        }

        return ordinal;
    }



}