package com.watxaut.insulinapp.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.graphics.Palette;
import android.util.Log;
import android.util.SparseArray;
import android.widget.ImageView;

import com.watxaut.insulinapp.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class GlobalFunctions {
    Context context;

    public GlobalFunctions(Context context){
        this.context = context;

    }

    public android.support.v7.app.AlertDialog.Builder fesConnexioBuilder() {

        return new android.support.v7.app.AlertDialog.Builder(context)
                .setTitle(context.getString(R.string.titolNoInternet))
                .setMessage(context.getString(R.string.cosNoInternet))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setCancelable(false);
    }

    public android.support.v7.app.AlertDialog.Builder fesSimpleBuilder(String titol, String cos) {

        return new android.support.v7.app.AlertDialog.Builder(context)
                .setTitle(titol)
                .setMessage(cos)
                .setCancelable(false);
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    public int passaDrawable(String avatar){
        switch (avatar){
            case "bismut": return R.drawable.avatar_bismut;
            case "boira": return R.drawable.avatar_boira;
            case "cors": return R.drawable.avatar_cors;
            case "daus": return R.drawable.avatar_daus;
            case "edifici": return R.drawable.avatar_edifici;
            case "fulles": return R.drawable.avatar_fulles;
            case "globo": return R.drawable.avatar_globo;
            case "hall": return R.drawable.avatar_hall;
            case "sol": return R.drawable.avatar_sol;

            default: return R.drawable.cono;
        }

    }

    public void fesPalettes(){

        final Singleton singleton = Singleton.getInstance();
        for (final String avatar: retornaLlistaAvatars()) {
            int d = passaDrawable(avatar);
            Bitmap photo = BitmapFactory.decodeResource(context.getResources(), d);

            Palette.from(photo).generate(new Palette.PaletteAsyncListener() {
                public void onGenerated(Palette p) {
                    int bgColor = p.getMutedColor(context.getResources().getColor(android.R.color.black));
                    singleton.addLlistaPalettes(avatar, bgColor);
                }
            });
        }
    }

    public void setPalettesNegres(){
        final Singleton singleton = Singleton.getInstance();
        for (final String avatar: retornaLlistaAvatars()) {
            singleton.addLlistaPalettes(avatar, context.getResources().getColor(android.R.color.black));
        }

    }

    public List<String> retornaLlistaAvatars(){

        return Arrays.asList("bismut", "boira", "cors", "daus", "edifici", "fulles", "globo", "hall", "sol");
    }

    public String retornaRandomString(){
        Random r = new Random();
        int randomInt = r.nextInt(retornaLlistaAvatars().size());
        return retornaLlistaAvatars().get(randomInt);
    }

    public void fesDescendenciaiAscendencia(List<List<Integer>> llistaAscendencia){

        SparseArray<List<Integer>> descendencies = new SparseArray<>();
        SparseArray<List<Integer>> ascendencies = new SparseArray<>();
        int nivell = 1;

        while(nivell<=llistaAscendencia.size()){
            ascendencies.put(nivell, llistaAscendencia.get(nivell-1));
            descendencies.put(nivell, new ArrayList<Integer>());
            nivell++;
        }

        for(int i = 0; i < ascendencies.size(); i++) {
            nivell = ascendencies.keyAt(i);
            // get the object by the key.

            List<Integer> llistaAscendenciesNivell = ascendencies.get(nivell);
            for(int nivellAscendencia: llistaAscendenciesNivell){
                List<Integer> llistaDescendenciesNivell = descendencies.get(nivellAscendencia);
                llistaDescendenciesNivell.add(nivell);
                descendencies.put(nivellAscendencia,llistaDescendenciesNivell);
            }
        }

        Singleton singleton = Singleton.getInstance();
        singleton.setAscendencies(ascendencies);
        singleton.setDescendencies(descendencies);

    }

    public enum AppStart {
        FIRST_TIME, FIRST_TIME_VERSION, NORMAL;
    }

    /**
     * The app version code (not the version name!) that was used on the last
     * start of the app.
     */
    private static final String LAST_APP_VERSION = "last_app_version";

    /**
     * Finds out started for the first time (ever or in the current version).<br/>
     * <br/>
     * Note: This method is <b>not idempotent</b> only the first call will
     * determine the proper result. Any subsequent calls will only return
     * {@link AppStart#NORMAL} until the app is started again. So you might want
     * to consider caching the result!
     *
     * @return the type of app start
     */
    public AppStart checkAppStart() {
        PackageInfo pInfo;
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(context);
        AppStart appStart = AppStart.NORMAL;
        try {
            pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            int lastVersionCode = sharedPreferences
                    .getInt(LAST_APP_VERSION, -1);
            int currentVersionCode = pInfo.versionCode;
            appStart = checkAppStart(currentVersionCode, lastVersionCode);
            // Update version in preferences
            sharedPreferences.edit()
                    .putInt(LAST_APP_VERSION, currentVersionCode).apply();
        } catch (PackageManager.NameNotFoundException e) {
            Log.w("CheckFirstTime",
                    "Unable to determine current app version from pacakge manager. Defenisvely assuming normal app start.");
        }
        return appStart;
    }

    public AppStart checkAppStart(int currentVersionCode, int lastVersionCode) {
        if (lastVersionCode == -1) {
            return AppStart.FIRST_TIME;
        } else if (lastVersionCode < currentVersionCode) {
            return AppStart.FIRST_TIME_VERSION;
        } else if (lastVersionCode > currentVersionCode) {
            Log.w("CheckFirstTime", "Current version code (" + currentVersionCode
                    + ") is less then the one recognized on last startup ("
                    + lastVersionCode
                    + "). Defenisvely assuming normal app start.");
            return AppStart.NORMAL;
        } else {
            return AppStart.NORMAL;
        }
    }
}
