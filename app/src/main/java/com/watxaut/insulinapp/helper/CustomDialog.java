package com.watxaut.insulinapp.helper;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ProgressBar;

import com.parse.ParseUser;
import com.watxaut.insulinapp.R;

public class CustomDialog extends android.support.v4.app.DialogFragment {

    private int experiencia = 300;
    private int experienciaASumar = 100;
    private int puntsText = 10000;
    private int puntsASumar = 2200;
    private int expNivell = 5000;

    ProgressBar progressBar;
    com.uguratar.countingtextview.countingTextView textViewExpASumar;
    com.uguratar.countingtextview.countingTextView textViewPunts;
    com.uguratar.countingtextview.countingTextView textViewPuntsASumar;


    public static CustomDialog newInstance(int exp, int expSuma, int punts, int puntsSuma, int expNivell) {
        CustomDialog f = new CustomDialog();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("exp", exp);
        args.putInt("expSuma", expSuma);
        args.putInt("punts", punts);
        args.putInt("puntsSuma", puntsSuma);
        args.putInt("expNivell", expNivell);

        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);

        experiencia = getArguments().getInt("exp");
        experienciaASumar = getArguments().getInt("expSuma");
        puntsText = getArguments().getInt("punts");
        puntsASumar = getArguments().getInt("puntsSuma");
        expNivell = getArguments().getInt("expNivell");

        final int expInicial = experiencia;
        final int expNivellInicial = expNivell;

        final Singleton singleton = Singleton.getInstance();
        final boolean[] levelUp = {false};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_game_level_screen, null);

        progressBar = (ProgressBar) view.findViewById(R.id.progressBarExperiencia);
        textViewExpASumar = (com.uguratar.countingtextview.countingTextView) view.findViewById(R.id.experienciaASumar);
        textViewPunts = (com.uguratar.countingtextview.countingTextView) view.findViewById(R.id.puntsTotals);
        textViewPuntsASumar = (com.uguratar.countingtextview.countingTextView) view.findViewById(R.id.puntsASumar);

        progressBar.setMax(expNivell);
        progressBar.setProgress(experiencia);
        textViewExpASumar.setText(String.valueOf(experienciaASumar));
        textViewPunts.setText(String.valueOf(puntsText));
        textViewPuntsASumar.setText(String.valueOf(puntsASumar));

        textViewExpASumar.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (progressBar.getProgress() < expNivell) {
                    if (!levelUp[0]) {
                        progressBar.setProgress(experiencia + (experienciaASumar - Integer.parseInt(s.toString())));
                    } else {
                        progressBar.setProgress(experienciaASumar - (expNivellInicial - expInicial) - Integer.parseInt(s.toString()));
                    }
                } else {
                    if (singleton.getLlistaRangPunts().size() != singleton.getNivellUsuari() + 1) {
                        progressBar.setProgress(0);
                        int nivell = singleton.getNivellUsuari() + 1;
                        expNivell = singleton.getLlistaRangPunts().get(nivell - 1);
                        progressBar.setMax(expNivell);
                        experiencia = 0;
                        levelUp[0] = true;
                    }
                }
            }
        });

        int secondsDelayed = 1;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                textViewPuntsASumar.setStartValue(puntsASumar);
                textViewPuntsASumar.setEndValue(0);
                textViewPuntsASumar.animateText(1500);
                textViewExpASumar.setStartValue(experienciaASumar);
                textViewExpASumar.setEndValue(0);
                textViewExpASumar.animateText(2500);
                textViewPunts.setStartValue(puntsText);
                textViewPunts.setEndValue(puntsASumar + puntsText);
                textViewPunts.animateText(1500);
                //progressBar.setProgress(experiencia + experienciaASumar);
            }
        }, secondsDelayed * 1000);

        builder.setView(view)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        ParseUser user = ParseUser.getCurrentUser();
                        if (levelUp[0]) {
                            user.increment("NivellUsuari");
                            singleton.setNivellUsuari(singleton.getNivellUsuari() + 1);
                            user.put("Experiencia", experienciaASumar - (expNivellInicial - expInicial));
                            singleton.setExperiencia(experienciaASumar - (expNivellInicial - expInicial));
                        } else {
                            user.put("Experiencia", expInicial + experienciaASumar);
                            singleton.setExperiencia(expInicial + experienciaASumar);
                        }
                        user.saveEventually();
                        dismiss();
                    }
                });
        return builder.create();
    }

}
