package com.watxaut.insulinapp.helper;

import android.util.SparseArray;

import com.parse.ParseObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Singleton {
    private static Singleton instance = null;

    // Exists only to defeat instantation
    protected Singleton() {}
    public static Singleton getInstance() {
        if(instance == null){
            instance = new Singleton();
        }
        return instance;
    }
    //Variables User
    private String nickname = "";
    private String nom = "";
    private String cognoms = "";
    private int nivellActual = 1;
    private String avatar = "";
    private int experiencia = 0;
    private int nivellUsuari = 0;
    private List<ParseObject> llistaNivells;
    private List llistaWorlds;
    private List llistaRangNoms;
    private List llistaRangPunts;
    private List<Integer> nivellsPassats;

    private Map<String,Integer> llistaPalettes = new HashMap<>();

    private SparseArray<List<Integer>> ascendencies;
    private SparseArray<List<Integer>> descendencies;

    private boolean retornaDeGame = false;
    private boolean haGuanyat = false;
    private int nivellSeleccionat = 1;
    private int puntuacioPartida = 0;

    //Getters i Setters
    public String getNickname() {
        return nickname;
    }
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getCognoms() {
        return cognoms;
    }

    public void setCognoms(String cognoms) {
        this.cognoms = cognoms;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public boolean isRetornaDeGame() {
        return retornaDeGame;
    }

    public void setRetornaDeGame(boolean retornaDeGame) {
        this.retornaDeGame = retornaDeGame;
    }

    public boolean isHaGuanyat() {
        return haGuanyat;
    }

    public void setHaGuanyat(boolean haGuanyat) {
        this.haGuanyat = haGuanyat;
    }

    public int getNivellActual() {
        return nivellActual;
    }

    public void setNivellActual(int nivellActual) {
        this.nivellActual = nivellActual;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getNivellSeleccionat() {
        return nivellSeleccionat;
    }

    public void setNivellSeleccionat(int nivellSeleccionat) {
        this.nivellSeleccionat = nivellSeleccionat;
    }
    public List<ParseObject> getLlistaNivells() {
        return llistaNivells;
    }

    public void setLlistaNivells(List<ParseObject> llistaNivells) {
        this.llistaNivells = llistaNivells;
    }

    public List<List<Integer>> getLlistaWorlds() {
        return llistaWorlds;
    }

    public void setLlistaWorlds(List<List<Integer>> llistaWorlds) {
        this.llistaWorlds = llistaWorlds;
    }

    public int getExperiencia() {
        return experiencia;
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public int getNivellUsuari() {
        return nivellUsuari;
    }

    public void setNivellUsuari(int nivellUsuari) {
        this.nivellUsuari = nivellUsuari;
    }

    public int getPuntuacioPartida() {
        return puntuacioPartida;
    }

    public void setPuntuacioPartida(int puntuacioPartida) {
        this.puntuacioPartida = puntuacioPartida;
    }

    public List<String> getLlistaRangNoms() {
        return llistaRangNoms;
    }

    public void setLlistaRangNoms(List<String> llistaRangNoms) {
        this.llistaRangNoms = llistaRangNoms;
    }

    public List<Integer> getLlistaRangPunts() {
        return llistaRangPunts;
    }

    public void setLlistaRangPunts(List<Integer> llistaRangPunts) {
        this.llistaRangPunts = llistaRangPunts;
    }

    public List<Integer> getNivellsPassats() {
        return nivellsPassats;
    }

    public void setNivellsPassats(List<Integer> nivellsPassats) {
        this.nivellsPassats = nivellsPassats;
    }

    public void addNivellsPassats(int nivellPassat){
        this.nivellsPassats.add(nivellPassat);
    }

    public SparseArray<List<Integer>> getDescendencies() {
        return descendencies;
    }

    public void setDescendencies(SparseArray<List<Integer>> descendencies) {
        this.descendencies = descendencies;
    }

    public SparseArray<List<Integer>> getAscendencies() {
        return ascendencies;
    }

    public void setAscendencies(SparseArray<List<Integer>> ascendencies) {
        this.ascendencies = ascendencies;
    }

    public Map<String, Integer> getLlistaPalettes() {
        return llistaPalettes;
    }

    public void setLlistaPalettes(Map<String, Integer> llistaPalettes) {
        this.llistaPalettes = llistaPalettes;
    }
    public void addLlistaPalettes(String avatar,int palette){
        this.llistaPalettes.put(avatar, palette);

    }


}