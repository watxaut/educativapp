package com.watxaut.insulinapp.helper;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.watxaut.insulinapp.R;

public class CustomFABLayout extends RelativeLayout {

    LayoutInflater mInflater;
    public CustomFABLayout(Context context) {
        super(context);
        mInflater = LayoutInflater.from(context);
        init();

    }
    public CustomFABLayout(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        mInflater = LayoutInflater.from(context);
        init();
    }
    public CustomFABLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        mInflater = LayoutInflater.from(context);
        init();
    }
    public void init()
    {
        mInflater.inflate(R.layout.fab_num, this, true);
    }
}