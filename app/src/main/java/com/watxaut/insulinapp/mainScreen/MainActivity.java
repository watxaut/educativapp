package com.watxaut.insulinapp.mainScreen;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.parse.ParseUser;
import com.watxaut.insulinapp.R;
import com.watxaut.insulinapp.game.GameLevelActivity;
import com.watxaut.insulinapp.helper.GlobalFunctions;
import com.watxaut.insulinapp.helper.Singleton;
import com.watxaut.insulinapp.login.StartupActivity;

public class MainActivity extends AppCompatActivity {
    GlobalFunctions gf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gf = new GlobalFunctions(this);


        TextView benvinguda = (TextView) findViewById(R.id.benvinguda);
        final Singleton singleton = Singleton.getInstance();
        benvinguda.setText(String.format(getResources().getString(R.string.benvingut), singleton.getNickname()));

    }

    //---------------------------Determina quin boto s'ha apretat i ves a la activitat en conseqüència------------------------//
    public void vesActivity(View view){
        int id = view.getId();
        switch (id) {
            case R.id.play:
                Singleton singleton = Singleton.getInstance();
                singleton.setRetornaDeGame(false);
                startActivity(new Intent(MainActivity.this, GameLevelActivity.class));
                break;
            case R.id.perfil:
                startActivity(new Intent(MainActivity.this, PerfilUserParallax.class));

                break;
            case R.id.highscores:
                startActivity(new Intent(MainActivity.this, HighScoresRecycler.class));
                break;
            default:
                break;
        }

        overridePendingTransition(R.anim.activity_slide_start_enter, R.anim.activity_scale_start_exit);
    }

    public void logOut(View view){

        startActivity(new Intent(MainActivity.this, StartupActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    @Override
    public void onBackPressed() {

        gf.fesSimpleBuilder(getString(R.string.titolTancaApp),getString(R.string.cosTancarApp))
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        ParseUser.logOutInBackground();
                        MainActivity.this.finish();
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    //---------------------------------------Funcions de barra de menus, igual em serveixen algun dia ----------------------------//
    /*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    */

}
