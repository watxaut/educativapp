package com.watxaut.insulinapp.mainScreen;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;
import android.widget.RelativeLayout;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.watxaut.insulinapp.R;
import com.watxaut.insulinapp.helper.GlobalFunctions;
import com.watxaut.insulinapp.helper.ListAdapter;
import com.watxaut.insulinapp.helper.ParseErrorHandler;
import com.watxaut.insulinapp.helper.Singleton;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HighScoresRecycler extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private StaggeredGridLayoutManager mStaggeredLayoutManager;
    private ListAdapter mAdapter;
    GlobalFunctions globalFunctions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        globalFunctions = new GlobalFunctions(this);

        setContentView(R.layout.highscores_recycler);

        mRecyclerView = (RecyclerView) findViewById(R.id.list);
        mStaggeredLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mStaggeredLayoutManager);

        //setContentView(R.layout.highscores_recycler);
        final RelativeLayout progressScreen = (RelativeLayout) findViewById(R.id.progressHighScoreRecycler);
        progressScreen.setVisibility(View.VISIBLE);

        //Si te connexio a internet
        if (globalFunctions.isOnline()) {

            ParseQuery<ParseUser> query = ParseUser.getQuery();
            query.orderByDescending("Puntuacio");
            query.findInBackground(new FindCallback<ParseUser>() {
                public void done(List<ParseUser> llistaUsuaris, ParseException e) {
                    if (e == null) {

                        Singleton singleton = Singleton.getInstance();
                        mAdapter = new ListAdapter(HighScoresRecycler.this, llistaUsuaris, singleton.getLlistaPalettes());
                        mRecyclerView.setAdapter(mAdapter);
                    } else { //Alguna cosa ha anat malament
                        ParseErrorHandler.handleParseError(e, HighScoresRecycler.this);
                    }
                    progressScreen.setVisibility(View.GONE);
                }
            });
        }else{ //No hi ha connexio a internet
            globalFunctions.fesConnexioBuilder().show();
        }
    }

}
