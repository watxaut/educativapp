package com.watxaut.insulinapp.mainScreen;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parse.ParseUser;
import com.rey.material.widget.ProgressView;
import com.watxaut.insulinapp.R;
import com.watxaut.insulinapp.helper.GlobalFunctions;
import com.watxaut.insulinapp.helper.Singleton;


public class PerfilUserParallax extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.perfil_user_parallax);

        Singleton singleton = Singleton.getInstance();
        GlobalFunctions globalFunctions = new GlobalFunctions(this);

        TextView nomUsuari = (TextView) findViewById(R.id.nomUsuariParallax);
        nomUsuari.setText(singleton.getNickname());

        int id = globalFunctions.passaDrawable(singleton.getAvatar());
        Drawable d = getResources().getDrawable(id);
        ImageView i = (ImageView) findViewById(R.id.avatarPerfilParallax);
        i.setImageDrawable(d);

        final LinearLayout l = (LinearLayout) findViewById(R.id.placeNameHolderParallax);

        Bitmap photo = BitmapFactory.decodeResource(getResources(), id);

        Palette.from(photo).generate(new Palette.PaletteAsyncListener() {
            public void onGenerated(Palette p) {
                int bgColor = p.getMutedColor(getResources().getColor(android.R.color.black));
                l.setBackgroundColor(bgColor);
            }
        });

        //FIRST CARD
        ProgressView progressLevel = (ProgressView) findViewById(R.id.progressUserLevel);
        TextView nivellUser = (TextView) findViewById(R.id.nivellUser);
        TextView rangUser = (TextView) findViewById(R.id.rangUser);

        //nivell comença per 1
        int nivellUsuari = singleton.getNivellUsuari();
        float experienciaNivell = singleton.getLlistaRangPunts().get(nivellUsuari - 1);
        progressLevel.setProgress((singleton.getExperiencia() * 1f/experienciaNivell));
        nivellUser.setText(String.format("%1$d", nivellUsuari));

        rangUser.setText(singleton.getLlistaRangNoms().get(nivellUsuari - 1));

        //SECOND CARD
        TextView nivellCard = (TextView) findViewById(R.id.nivellCard);
        TextView puntuacioCard = (TextView) findViewById(R.id.puntuacioCard);
        TextView experienciaCard = (TextView) findViewById(R.id.experienciaCard);
        TextView expTilNextLevel = (TextView) findViewById(R.id.expTilNextLevel);

        nivellCard.setText(String.format(getString(R.string.nivellCard), nivellUsuari));

        ParseUser user = ParseUser.getCurrentUser();
        puntuacioCard.setText(String.format(getString(R.string.puntuacioCard), user.getInt("Puntuacio")));
        int exp = user.getInt("Experiencia");
        experienciaCard.setText(String.format(getString(R.string.expCard), exp));
        int expNivell = singleton.getLlistaRangPunts().get(nivellUsuari - 1);
        expTilNextLevel.setText(String.format(getString(R.string.expTilNextLevel), expNivell - exp));
    }

}
