package com.watxaut.insulinapp.game;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.rey.material.app.ThemeManager;
import com.watxaut.insulinapp.R;
import com.watxaut.insulinapp.helper.GlobalFunctions;
import com.watxaut.insulinapp.helper.ParseErrorHandler;
import com.watxaut.insulinapp.helper.Singleton;

import java.util.List;
import java.util.Locale;

public class GameScreen extends AppCompatActivity {

    List<ParseObject> llistaPreguntes;
    String respostaCorrecte = "";
    Button a;
    Button b;
    Button c;
    Button d;
    Button at;
    Button bt;
    Button ct;
    Button dt;
    ImageButton ai;
    ImageButton bi;
    ImageButton ci;
    ImageButton di;
    ImageView imageT;
    ImageView cor1;
    ImageView cor2;
    ImageView cor3;
    TextView enunciat;
    TextView enunciatT;
    TextView enunciatI;
    ParseObject pregunta;
    Singleton singleton;

    RelativeLayout gameText;
    LinearLayout gameImages;
    LinearLayout gamesTextImage;

    int contadorProgress = 0;
    int puntuacioPartida = 0;
    CountDownTimer mCountDownTimer;
    int segonsNivell;

    GlobalFunctions globalFunctions;
    boolean isOnPause = false;
    boolean isError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeManager.init(this, 1, 0, null);
        setContentView(R.layout.game_main);

        globalFunctions = new GlobalFunctions(this);
        singleton = Singleton.getInstance();

        gameText = (RelativeLayout) findViewById(R.id.game_text);
        gameImages = (LinearLayout) findViewById(R.id.game_images);
        gamesTextImage = (LinearLayout) findViewById(R.id.game_text_images);

        final RelativeLayout progressScreen = (RelativeLayout) findViewById(R.id.progressGameScreen);

        progressScreen.setVisibility(View.VISIBLE);

        if (globalFunctions.isOnline()) {
            cor1 = (ImageView) findViewById(R.id.imageView1);
            cor2 = (ImageView) findViewById(R.id.imageView2);
            cor3 = (ImageView) findViewById(R.id.imageView3);

            a = (Button) findViewById(R.id.button);
            b = (Button) findViewById(R.id.button2);
            c = (Button) findViewById(R.id.button3);
            d = (Button) findViewById(R.id.button4);

            at = (Button) findViewById(R.id.buttont);
            bt = (Button) findViewById(R.id.buttont2);
            ct = (Button) findViewById(R.id.buttont3);
            dt = (Button) findViewById(R.id.buttont4);
            imageT = (ImageView) findViewById(R.id.imageT);

            ai = (ImageButton) findViewById(R.id.imageA);
            bi = (ImageButton) findViewById(R.id.imageB);
            ci = (ImageButton) findViewById(R.id.imageC);
            di = (ImageButton) findViewById(R.id.imageD);

            a.setEnabled(false); b.setEnabled(false); c.setEnabled(false); d.setEnabled(false);
            at.setEnabled(false); bt.setEnabled(false); ct.setEnabled(false); dt.setEnabled(false);
            ai.setEnabled(false); bi.setEnabled(false); ci.setEnabled(false); di.setEnabled(false);

            enunciat = (TextView) findViewById(R.id.pregunta);
            enunciatT = (TextView) findViewById(R.id.preguntaT);
            enunciatI = (TextView) findViewById(R.id.preguntaI);

            segonsNivell = singleton.getLlistaNivells().get(singleton.getNivellSeleccionat()-1).getInt("Temps");

            String loc = Locale.getDefault().getLanguage();
            String idiomaPreguntes;
            switch(loc){
                case "ca":
                    idiomaPreguntes = "Preguntes";
                    break;
                case "es":
                    idiomaPreguntes = "Preguntes_es";
                    break;
                default:
                    idiomaPreguntes = "Preguntes";
                    break;
            }

            ParseQuery<ParseObject> query = ParseQuery.getQuery(idiomaPreguntes);
            query.whereEqualTo("Nivell", singleton.getNivellSeleccionat());
            query.findInBackground(new FindCallback<ParseObject>() {
                public void done(List<ParseObject> llistaParse, ParseException e) {
                    if (e == null) {
                        if (llistaParse.size()==0){
                            Toast.makeText(GameScreen.this, getResources().getString(R.string.whoops), Toast.LENGTH_LONG).show();
                            singleton.setRetornaDeGame(true);
                            singleton.setHaGuanyat(false);
                            isError = true;
                            //startActivity(new Intent(GameScreen.this, GameLevelActivity.class));
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            finish();
                        }else {
                            llistaPreguntes = llistaParse;
                            pregunta = getPregunta();
                            tipusPregunta(pregunta.getString("TipusPregunta"));

                            progressScreen.setVisibility(View.GONE);
                            setUpProgressBar(segonsNivell);
                        }

                    } else {
                        if (e.getCode() == ParseException.INVALID_SESSION_TOKEN) {
                            singleton.setRetornaDeGame(false);
                            singleton.setHaGuanyat(false);
                            isError = true;
                            ParseErrorHandler.handleParseError(e, GameScreen.this);
                        } else {
                            Toast.makeText(GameScreen.this, getResources().getString(R.string.whoops), Toast.LENGTH_LONG).show();
                            singleton.setRetornaDeGame(true);
                            singleton.setHaGuanyat(false);
                            isError = true;
                            //startActivity(new Intent(GameScreen.this, GameLevelActivity.class));
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            finish();
                        }

                    }
                }
            });

        }else{
            globalFunctions.fesSimpleBuilder(getResources().getString(R.string.titolNoInternet), getResources().getString(R.string.cosNoInternet))
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            singleton.setRetornaDeGame(true);
                            singleton.setHaGuanyat(false);
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                            finish();
                        }
                    }).show();
        }
    }


    public void tipusPregunta(String tipus){
        switch (tipus){
            case "Text":
                enunciat.setText(pregunta.getString("Text"));
                gameText.setVisibility(View.VISIBLE);
                gameImages.setVisibility(View.GONE);
                gamesTextImage.setVisibility(View.GONE);

                a.setText(pregunta.getString("R1"));
                b.setText(pregunta.getString("R2"));
                c.setText(pregunta.getString("R3"));
                d.setText(pregunta.getString("R4"));

                a.setEnabled(true);
                b.setEnabled(true);
                c.setEnabled(true);
                d.setEnabled(true);
                break;
            case "ImageText":
                enunciatT.setText(pregunta.getString("Text"));
                imageT.setImageDrawable(getImage(pregunta.getString("imatge")));

                gameText.setVisibility(View.GONE);
                gameImages.setVisibility(View.GONE);
                gamesTextImage.setVisibility(View.VISIBLE);

                at.setText(pregunta.getString("R1"));
                bt.setText(pregunta.getString("R2"));
                ct.setText(pregunta.getString("R3"));
                dt.setText(pregunta.getString("R4"));

                at.setEnabled(true);
                bt.setEnabled(true);
                ct.setEnabled(true);
                dt.setEnabled(true);
                break;
            case "Image":
                enunciatI.setText(pregunta.getString("Text"));

                gameText.setVisibility(View.GONE);
                gameImages.setVisibility(View.VISIBLE);
                gamesTextImage.setVisibility(View.GONE);

                ai.setImageDrawable(getImage(pregunta.getString("R1")));
                bi.setImageDrawable(getImage(pregunta.getString("R2")));
                ci.setImageDrawable(getImage(pregunta.getString("R3")));
                di.setImageDrawable(getImage(pregunta.getString("R4")));

                ai.setEnabled(true);
                bi.setEnabled(true);
                ci.setEnabled(true);
                di.setEnabled(true);
                break;
        }

        respostaCorrecte = pregunta.getString("RespostaCorrecte");
    }

    public Drawable getImage(String image){
        switch (image){
            case "Chocolate":
                return ContextCompat.getDrawable(this, R.drawable.image_chocolate);
            case "Xocolata":
                return ContextCompat.getDrawable(this, R.drawable.image_chocolate);
            case "Tomaquets":
                return ContextCompat.getDrawable(this, R.drawable.image_tomaquets);
            case "Tomates":
                return ContextCompat.getDrawable(this, R.drawable.image_tomaquets);
            case "Llenties":
                return ContextCompat.getDrawable(this, R.drawable.image_llenties);
            case "Lentejas":
                return ContextCompat.getDrawable(this, R.drawable.image_llenties);
            case "Arros":
                return ContextCompat.getDrawable(this, R.drawable.image_arros);
            case "Arroz":
                return ContextCompat.getDrawable(this, R.drawable.image_arros);
            default:
                return ContextCompat.getDrawable(this, R.drawable.cono);
        }

    }

    public ParseObject getPregunta(){
        ParseObject pregunta = llistaPreguntes.get(0);
        llistaPreguntes.remove(0);
        return pregunta;
    }

    public void seguentPregunta(){
        pregunta = getPregunta();
        tipusPregunta(pregunta.getString("TipusPregunta"));

    }

    public void usuariHaTriat(final View view){
        String respostaSeleccionada;
        boolean botoText = true;
        if (gameText.getVisibility() == View.VISIBLE){
            Button botoSeleccionat = (Button) view;
            respostaSeleccionada = botoSeleccionat.getText().toString();
            a.setEnabled(false);
            b.setEnabled(false);
            c.setEnabled(false);
            d.setEnabled(false);

        }else if(gamesTextImage.getVisibility() == View.VISIBLE){
            Button botoSeleccionat = (Button) view;
            respostaSeleccionada = botoSeleccionat.getText().toString();
            at.setEnabled(false);
            bt.setEnabled(false);
            ct.setEnabled(false);
            dt.setEnabled(false);

        }else{
            //GameImage
            botoText = false;
            ImageButton botoSeleccionat = (ImageButton) view;
            respostaSeleccionada = getNomDeLaImatge(botoSeleccionat.getId());



            ai.setEnabled(false);
            bi.setEnabled(false);
            ci.setEnabled(false);
            di.setEnabled(false);
        }

        ParseObject respostes = new ParseObject("Respostes");

        respostes.put("Pregunta", pregunta);
        respostes.put("NomUsuari", singleton.getNom());
        respostes.put("CognomsUsuari", singleton.getCognoms());
        respostes.put("UserName", singleton.getNickname());
        respostes.put("RespostaEscollida", respostaSeleccionada);
        respostes.put("RespostaCorrecte", pregunta.getString("RespostaCorrecte"));
        respostes.put("TextPregunta", pregunta.getString("Text"));

        if (respostaCorrecte.equals(respostaSeleccionada)){
            puntuacioPartida = puntuacioPartida + 100;
            if (botoText) {
                Button botoSeleccionat = (Button) view;
                botoSeleccionat.setBackgroundResource(R.drawable.button_shape_green);
            }
            respostes.put("esRespostaCorrecte", "Si");

        }else{
            if (botoText) {
                Button botoSeleccionat = (Button) view;
                botoSeleccionat.setBackgroundResource(R.drawable.button_shape_red);
            }else{
                ImageButton botoSeleccionat = (ImageButton) view;
                botoSeleccionat.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.image_button_wrong_answer));
            }
            respostes.put("esRespostaCorrecte", "No");
            treuVida();
        }

        respostes.saveEventually();

        int secondsDelayed = 1;
        final boolean finalBotoText = botoText;
        new Handler().postDelayed(new Runnable() {
            public void run() {
                if (finalBotoText) {
                    Button botoSeleccionat = (Button) view;
                    botoSeleccionat.setBackgroundResource(R.drawable.button_shape);
                }
                if (llistaPreguntes.size() != 0) {
                    seguentPregunta();
                    if (gameText.getVisibility() == View.VISIBLE) {
                        a.setEnabled(true);
                        b.setEnabled(true);
                        c.setEnabled(true);
                        d.setEnabled(true);

                    } else if (gamesTextImage.getVisibility() == View.VISIBLE) {
                        at.setEnabled(true);
                        bt.setEnabled(true);
                        ct.setEnabled(true);
                        dt.setEnabled(true);

                    } else {
                        ai.setEnabled(true);
                        bi.setEnabled(true);
                        ci.setEnabled(true);
                        di.setEnabled(true);
                    }

                } else {
                    returnToLevel(true);
                    //startActivity(new Intent(GameScreen.this, MainActivity.class));
                    //finish();
                }
            }
        }, secondsDelayed * 500);

        if (cor1.getVisibility() == View.INVISIBLE && cor2.getVisibility() == View.INVISIBLE && cor3.getVisibility() == View.INVISIBLE){
            //Atura el joc
            Toast.makeText(GameScreen.this, getResources().getString(R.string.noLifes), Toast.LENGTH_SHORT).show();
            returnToLevel(false);
        }

    }


    public String getNomDeLaImatge(int id){
        String nom;
        switch (id){
            case R.id.imageA: nom = pregunta.getString("R1");
                break;
            case R.id.imageB: nom = pregunta.getString("R2");
                break;
            case R.id.imageC: nom = pregunta.getString("R3");
                break;
            case R.id.imageD: nom = pregunta.getString("R4");
                break;
            default:
                nom = "Algo ha anat malament";
                break;
        }
        return nom;
    }


    public void treuVida(){
        if (cor1.getVisibility() == View.VISIBLE){
            cor1.setVisibility(View.INVISIBLE);
        }else if (cor2.getVisibility() == View.VISIBLE) {
            cor2.setVisibility(View.INVISIBLE);
        }else{
            cor3.setVisibility(View.INVISIBLE);
        }
    }

    public void returnToLevel(boolean haGuanyat){
        //gestiona el nivell i retorna al boto que toca

        if (haGuanyat) {

            puntuacioPartida = puntuacioPartida + ((segonsNivell*10) - contadorProgress) * 100;
            singleton.setPuntuacioPartida(puntuacioPartida);
        }

        mCountDownTimer.cancel();
        singleton.setRetornaDeGame(true);
        singleton.setHaGuanyat(haGuanyat);
        //startActivity(new Intent(GameScreen.this, GameLevelActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    public void setUpProgressBar(int segons){
        final ProgressBar mProgressBar;

        mProgressBar=(ProgressBar)findViewById(R.id.progressbar);
        mProgressBar.setMax(segonsNivell * 10);
        mProgressBar.setProgress(contadorProgress);

        mCountDownTimer=new CountDownTimer(segons*1000,100) {

            @Override
            public void onTick(long millisUntilFinished) {
               // int red = (int)((segonsNivell - millisUntilFinished/1000f)*255f/segonsNivell);
                //int blue = (int)((millisUntilFinished/1000f)*255f/segonsNivell);
                //mProgressBar.setBackgroundColor(Color.rgb(red, 20, blue));
                contadorProgress++;
                mProgressBar.setProgress(contadorProgress);
            }

            @Override
            public void onFinish() {
                //Do what you want
                //TODO BOOOm
                Toast.makeText(GameScreen.this, "BOOM!", Toast.LENGTH_SHORT).show();
                contadorProgress = 0;
                returnToLevel(false);
            }
        };
        mCountDownTimer.start();

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isError) {
            mCountDownTimer.cancel();
            isOnPause = true;
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (isOnPause){
            isOnPause = false;
            setUpProgressBar(segonsNivell - contadorProgress);
        }

    }

    @Override
    public void onBackPressed() {

        //TODO Pausar Joc
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.titolSortirNivell))
                .setMessage(getResources().getString(R.string.cosSortirNivell))
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        Singleton singleton = Singleton.getInstance();
                        singleton.setRetornaDeGame(true);
                        mCountDownTimer.cancel();
                        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        //startActivity(new Intent(GameScreen.this, GameLevelActivity.class));
                        finish();
                    }

                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }

                })
                .show();
    }

}
